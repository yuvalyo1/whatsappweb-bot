const CHAT = "_3j7s9"
const CHAT_NAME = "_25Ooe"
const BUTTON_SEND = "_35EW6"
const BUTTON_VOICE = "_1UWg0"
const TEXT_LINE = "_2S1VP copyable-text selectable-text"
const MESSAGE = "_3zb-j ZhF0n" 
const MESSAGE_PAGE = "_9tCEa"


function get_chats(){
	return document.getElementsByClassName(CHAT)
}

function get_chat_by_title(title){
	var chats = get_chats()
	for(var i=0;i<chats.length;i++){
		var chat_title = chats[i].getElementsByClassName(CHAT_NAME)
		var innerHTML = chat_title.item(0).innerHTML.toLowerCase()
		if(innerHTML.includes(title.toLowerCase())){
			return chats[i]
		}
	}
	return null
}

function send_msg(msg){
	// enter msg to input line
	var line = document.getElementsByClassName(TEXT_LINE).item(0)
	line.innerText = msg
	
	// change the voice button to send button
	updated = Object.keys(line)[1]
	document.getElementsByClassName(TEXT_LINE).item(0)[updated]["onInput"]("blabla")
	
	// click "send button"
	document.getElementsByClassName(BUTTON_SEND).item(0).click() // must have words in the output line
}

function get_latest_message(){
	var length = document.getElementsByClassName(MESSAGE)["length"]
	return document.getElementsByClassName(MESSAGE)[length-1].textContent
}

function get_weather(){
	// use api key and get the weather
	send_msg("getting weather")
}

function echo(){
	// echo everything but the word "echo"
	send_msg("echoing")
}

function parse_msg(msg){
	var options = {"weather":get_weather, "echo":echo}
	
	sentence = msg.toLowerCase().split(" ")
	command = sentence[0]
	
	try{
		options[command]()
	}
	catch(err){
		send_msg("unknown command")
	}
}

function menu(){
	send_msg("Hello, Welcome To My bot, Follow This instructions And You Will Become Better")
	send_msg("1. get weather - *weather*")
	send_msg("2. echo somthing - *echo*")
	send_msg("3. stop this botttt - *done*")
}

function start_bot(){
	var END_BOT = "done"
	var target_node = document.getElementsByClassName(MESSAGE_PAGE)[0]
	var config = { attributes: false, childList: true, subtree: false }
	
	//show menu
	menu()
	
	var callback = function(mutations_list, observer) {
			// wont cause endless loop
			observer.disconnect()
			
			var msg = get_latest_message()
			if(msg.toLowerCase() != END_BOT){
				parse_msg(msg)
				observer.observe(target_node, config)
			}
			else{
				// end the cycle
				observer.disconnect()
			}
			
	};	
	var observer = new MutationObserver(callback)
	observer.observe(target_node, config)
}

